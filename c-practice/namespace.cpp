/* use of namespaces */
#include <iostream>
#include <stdio.h>
namespace std1 {
    void cout(int n) {
        printf("%d\n", n);
    }
};
using namespace std1;
using namespace std;
int main() {
    int n;
    cin>>n;
    std1::cout(n);
}
