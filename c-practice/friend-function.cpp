/* upon object u can access private & protected members outside the class by (friend function)*/
#include <iostream>
#include <stdio.h>
class Your;
using namespace std;
class My {
    int a = 10;
    friend int main();
};

int main() {
    My obj;
    cout<<obj.a<<endl;
}
  