/* upon object u can access private & protected members outside the class by (friend class)*/
#include <iostream>
#include <stdio.h>
class Your;
using namespace std;
class My {
    int a = 10;
    friend Your;
};
class Your
{
    public:
        My o;
        void method() {
            cout << "method="<<o.a<<endl;
        };
};

int main() {
    Your obj;
    obj.method();
}
  