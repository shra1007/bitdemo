#include <iostream>
#include <stdio.h>
class Your;
using namespace std;

/*int main() {
    int a = 10;
    const int *ptr = &a;//pointer to a const int (int const *ptr = &a)
    cout<<++a<<endl;// Allowed
    *ptr++;//Not - Allowed 
    cout<<*ptr<<endl;
    int b = 20;
    //int *const ptr1 = &b;//const pointer
    //ptr1 = &a;
    const int *const ptr2 = &b;//const pointer to a const int
    // *ptr2++;
    // ptr2 = &a;
    b++;
    cout<<b<<endl;
    return 0;
}*/
class Student {
    public:
        int rno = 10;
        //if u want this method to stop changing the data members u can make 'const' at the end of method declaration.
        int getRno() const {
            rno++;
            return rno;
        };
};

int main() {
    Student obj;
    cout<<obj.getRno()<<endl;
}

  