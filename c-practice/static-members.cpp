/* static method can only access static (data/methods) members*/
#include <iostream>
#include <stdio.h>
using namespace std;
class My {
    public:
        int a;
        static int count;
    My() {
        a = count++;
    }
    static void m1() {
        cout<<"method m1()"<<endl;
    };
    static int getCount() {
        // cout<<a<<endl;
        My();
        m1();
        return count;
    };
};
int My::count = 0;
int main() {
    My obj;
    cout<<obj.getCount()<<endl;
    cout<<My::count<<endl;
}
  