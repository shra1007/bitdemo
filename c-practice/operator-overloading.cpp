/* operator overloading */
#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Complex {
    public :
    int real, img;
    Complex(int r1 = 0, int r2 = 0) {
        real = r1;
        img = r2;
    }
    Complex operator+(Complex temp) {
        Complex t1;
        t1.real = real + temp.real;
        t1.img = img + temp.img;
        return t1;
    }
};

int main()
{
    Complex c1(2, 3);
    Complex c2(4, 3);
    Complex c3 = c1 + c2;
    cout<<c3.real <<" "<<c3.img<<endl;
   
}